import createActions from 'app/src/utils/redux/createActions'

export default createActions([
	'ENTER_CREDENTIALS',
	'AUTH_ACTION'
])
