import createActions from 'app/src/utils/redux/createActions'

export default createActions([
	'AUTH_ACTION_SUCCESS',
	'AUTH_ACTION_FAILURE',
	'TRANSACTION_START',
	'TRANSACTION_SUCCESS',
	'TRANSACTION_FAILURE'
])
