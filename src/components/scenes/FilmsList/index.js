import React from 'react'
import {connect} from 'react-redux'

import { StyleSheet, View, ScrollView } from 'react-native'
import FilmCard from 'app/src/components/common/Card'

import filmListSelector from 'app/src/selectors/filmList'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export class FilmList extends React.Component {
	render() {
		const { filmList } = this.props
		return (
			<View style={styles.container}>
				<ScrollView>
					{filmList.map(film => (
						<FilmCard
							imageURL={film.image}
							text={film.text}
							description={film.description}
						/>
					))}
				</ScrollView>
			</View>
		)
	}
}

export default connect(filmListSelector)(FilmList)
