import React from 'react'
import {connect} from 'react-redux'

import { StyleSheet, Text, TextInput, View, Button } from 'react-native'

import authSelector from 'app/src/selectors/auth'
import authActions from 'app/src/actions/auth'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    height: 40,
    width: '90%',
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 8
  }
})

export class SignUp extends React.Component {
	render() {
		const { props } = this
		return (
			<View style={styles.container}>
				<Text>Sign Up</Text>
				{ !!props.errorMessage &&
					<Text style={{ color: 'red' }}>
						{props.errorMessage}
					</Text>
				}
				<TextInput
					placeholder="Email"
					autoCapitalize="none"
					style={styles.textInput}
					onChangeText={login => props.ENTER_CREDENTIALS({ login })}
					value={props.login}
				/>
				<TextInput
					secureTextEntry
					placeholder="Password"
					autoCapitalize="none"
					style={styles.textInput}
					onChangeText={password => props.ENTER_CREDENTIALS({ password })}
					value={props.password}
				/>
				<Button title="Sign Up" onPress={_ => props.AUTH_ACTION('signup')} />
			</View>
		)
	}
}

export default connect(authSelector, authActions)(SignUp)
