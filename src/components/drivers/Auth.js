import React from 'react'
import { AsyncStorage } from 'react-native'

import firebase from 'react-native-firebase'

const signIn = (login, password) => firebase.auth().signInWithEmailAndPassword(login, password)
const signUp = (login, password) => firebase.auth().createUserWithEmailAndPassword(login, password)

export default class Auth extends React.Component {
	constructor(props) {
		super(props)
	}

	componentDidMount(){
		signIn(this.props.login, this.props.password)
		.then(res => this.props.onSuccess(res))
		.catch(err => this.props.onFailure(err))
	}

	componentWillUpdate(nextProps){
		if(nextProps.action){
			let authMethod
			switch(nextProps.action){
				case 'login': authMethod = signIn; break;
				case 'signup': authMethod = signUp; break;
				default: authMethod = new Promise((resolve) => resolve())
			}
			authMethod(nextProps.login, nextProps.password)
			.then(res => nextProps.onSuccess(res))
			.catch(err => nextProps.onFailure(err))
		}
	}

	render(){
		return null
	}
}
