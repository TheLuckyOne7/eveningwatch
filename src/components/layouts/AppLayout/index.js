import React from 'react'
import { connect } from 'react-redux'
import { createRootNavigator } from 'app/src/routes'

import Auth from 'app/src/components/drivers/Auth'
import DB from 'app/src/components/drivers/DB'

import appLayoutSelector from 'app/src/selectors/appLayout'
import appLayoutActions from 'app/src/actions/appLayout'

export const AppLayout = ({user = {}, buffer = {}, AUTH_ACTION_SUCCESS, AUTH_ACTION_FAILURE, TRANSACTION_START, TRANSACTION_SUCCESS, TRANSACTION_FAILURE}) => {
	const RootNavigator = createRootNavigator(user.logedIn, user.isChecked);
	let layout = [
		<Auth action={user.action}
		  login={user.login}
		  password={user.password}
		  onSuccess={AUTH_ACTION_SUCCESS}
		  onFailure={AUTH_ACTION_FAILURE}
		  key="USER_AUTH_LAYER" />,
		<RootNavigator key="ROOT_NAVIGATOR" />
	]
	if(user.logedIn)
		layout.push(
			<DB buffer={buffer}
				onBeforeTransaction={TRANSACTION_START}
				onTransactionSuccess={TRANSACTION_SUCCESS}
				onTransactionFailure={TRANSACTION_FAILURE}
				key="DB_LAYER"
			/>
		)
	return layout
}

export default connect(appLayoutSelector, appLayoutActions)(AppLayout)
