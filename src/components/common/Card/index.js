import React from 'react'
import { View, Image, Text } from 'react-native'


export default ({imageURL, name, description}) => (
	<View>
		<Image
			source={imageURL}
		/>
		<Text>{name}</Text>
		<Text>{description}</Text>
	</View>
)
