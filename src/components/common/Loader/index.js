import React from 'react'
import { View, Text, ActivityIndicator } from 'react-native'

import style from './style'

export default class Loading extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Loading</Text>
        <ActivityIndicator size="large" />
      </View>
    )
  }
}
