import React from 'react'
import { Provider } from 'react-redux'

import AppLayout from './components/layouts/AppLayout'
import Loading from './components/scenes/Loading'
import Scenes from './routes'

import {createStore} from './store'
import {restoreState} from './utils/redux/store'

export default class App extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			restored: false,
			store: {}
		}
	}
	componentWillMount(){
		restoreState()
		.then(initialState => {
			this.setState({restored: true, store: createStore(initialState) })
		})
	}
	render() {
		return this.state.restored ?
		(
			<Provider store={this.state.store} >
				<AppLayout />
			</Provider>
		)
		:
		(
			<Loading />
		)
	}
}
