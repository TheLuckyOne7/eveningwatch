import { createStore as createReduxStore, applyMiddleware } from 'redux'
import axios from 'axios'
import axiosMiddleware from 'redux-axios-middleware'
import logger from 'redux-logger'

import reducer from './reducers'
import { saveState } from 'app/src/utils/redux/store'

const client = axios.create({})

const enhancers = applyMiddleware(axiosMiddleware(client), logger)

export function createStore(initialState = {}){
	let store =  createReduxStore(reducer, initialState, enhancers)
	store.subscribe(() => saveState(store.getState()))
	return store
}
