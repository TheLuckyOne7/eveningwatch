import { AsyncStorage } from 'react-native'

export function saveState(state) {
	AsyncStorage.setItem('state',
		JSON.stringify({
			...state
	}))
	.then(e => console.log('saveState', state))
	.catch(err => console.error(err))
}

export function restoreState(key) {
	return AsyncStorage.getItem('state')
	.then(state => {
		console.log('Restored State', state)
		if(state !== null)
			return JSON.parse(state)
		else
			return {}
	})
}
