export default function createReducer(initialState, handlers) {
	return function reducer(state = initialState, action, ...rest) {
	const handler = handlers[action.type]
		return handler ? handler(state, action, ...rest) : state
	}
}
