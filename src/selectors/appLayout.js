export default (state) => {
	return {
		user: state.user,
		buffer: state.data.buffer
	}
}
