export default (state) => ({
	login: state.user.login,
	password: state.user.password,
	errorMessage: state.user.errorMessage
})
