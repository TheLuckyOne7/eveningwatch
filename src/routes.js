import { createStackNavigator, createSwitchNavigator, createBottomTabNavigator } from 'react-navigation'

import Loading from 'app/src/components/scenes/Loading'

import LoginScreen from 'app/src/components/scenes/Login'
import Signup from 'app/src/components/scenes/Signup'

import SelectedFilms from 'app/src/components/scenes/MyList'
import AddNewFilm from 'app/src/components/scenes/AddFilm'
import FilmsList from 'app/src/components/scenes/FilmsList'
import FilmDetails from 'app/src/components/scenes/FilmDetails'
import Settings from 'app/src/components/scenes/Settings'

const SignedOut = createBottomTabNavigator({
	Login: {
		screen: LoginScreen,
		navigationOptions: {
			title: "Log In"
		}
	},
	Signup: {
		screen: Signup,
		navigationOptions: {
			title: "Sign Up"
		}
	}
})

const SignedIn = createBottomTabNavigator({
	SelectedFilms: {
		screen: createStackNavigator({
			SelectedFilms: {
				screen: SelectedFilms,
				navigationOptions: {
					title: "My Film List"
				}
			},
			AddNewFilm: {
				screen: AddNewFilm,
				navigationOptions: {
					title: "Add New"
				}
			},
			FilmDetails: {
				screen: FilmDetails
			}
		},
		{
			initialRouteName: 'SelectedFilms'
		})
	},
	FilmsList: {
		screen: createStackNavigator({
			FilmsList: {
				screen: FilmsList
			},
			FilmDetails: {
				screen: FilmDetails
			}
		},
		{
			initialRouteName: 'FilmsList'
		})
	},
	Settings: {
		screen: Settings,
		navigationOptions: {
			title: "Settings"
		}
	}
})

export const createRootNavigator = (signedIn = false, checked = false) => {
	if(!checked)
	return Loading
	return createSwitchNavigator(
		{
			SignedIn: {
				screen: SignedIn
			},
			SignedOut: {
				screen: SignedOut
			}
		},
		{
			initialRouteName: signedIn ? "SignedIn" : "SignedOut"
		}
	)
};
