import createReducer from 'app/src/utils/redux/createReducer'

const initialState = {
	login: '',
	password: '',
	isChecked: false,
	logedIn: false,
	action: '',
	errorMessage: ''
}

export default createReducer(initialState, {
	'ENTER_CREDENTIALS': (state, action) => ({...state, ...action.data}),
	'AUTH_ACTION': (state, action) => ({...state, action: action.data}),
	'AUTH_ACTION_SUCCESS': (state, action) => ({...state, isChecked: true, logedIn: true, action: ''}),
	'AUTH_ACTION_FAILURE': (state, action) => ({...state, isChecked: true, action: '', errorMessage: action.data.message}),
})
