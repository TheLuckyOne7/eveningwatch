import createReducer from 'app/src/utils/redux/createReducer'

const initialState = {
	loading: false,
	db: {},
	buffer: {
		done: false
	}
}

export default createReducer(initialState, {
	'LOAD': (state) => ({...state, loading: true}),
	'LOAD_SUCCESS': (state) => ({...state, loading: false}),
	'LOAD_FAILURE': (state) => ({...state, loading: false})
})
